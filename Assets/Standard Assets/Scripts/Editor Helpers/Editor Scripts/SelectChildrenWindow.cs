#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using Extensions;
using System.Collections;
using System.Collections.Generic;

public class SelectChildrenWindow : EditorWindow
{
	public IntRange childSiblingIndicesRange = new IntRange(0, 0);

	[MenuItem("Window/Select Children")]
	static void Init()
	{
		SelectChildrenWindow window = (SelectChildrenWindow) EditorWindow.GetWindow(typeof(SelectChildrenWindow));
		window.Show();
	}

	void OnGUI ()
	{
		FloatRange childSiblingIndicesRange = new FloatRange(this.childSiblingIndicesRange.min, this.childSiblingIndicesRange.max);
		EditorGUILayout.LabelField("Min child sibling index: " + childSiblingIndicesRange.min);
        EditorGUILayout.LabelField("Max child sibling index: " + childSiblingIndicesRange.max);
		int maxChildCount = 0;
		Transform[] selectedTransforms = SelectionExtensions.GetSelected<Transform>();
		for (int i = 0; i < selectedTransforms.Length; i ++)
		{
			Transform selectedTrs = selectedTransforms[i];
			maxChildCount = Mathf.Max(selectedTrs.childCount, maxChildCount);
		}
		EditorGUILayout.MinMaxSlider(ref childSiblingIndicesRange.min, ref childSiblingIndicesRange.max, 0, maxChildCount);
		if (GUILayout.Button("Select"))
		{
			Selection.objects = new Object[0];
			for (int i = 0; i < selectedTransforms.Length; i ++)
			{
				Transform selectedTrs = selectedTransforms[i];
				for (int i2 = (int) childSiblingIndicesRange.min; i2 <= (int) childSiblingIndicesRange.max; i2 ++)
				{
					if (selectedTrs.childCount > i2)
					{
						Transform child = selectedTrs.GetChild(i2);
						Selection.objects = Selection.objects.Add(child.gameObject);
					}
				}
			}
		}
		this.childSiblingIndicesRange = new IntRange((int) childSiblingIndicesRange.min, (int) childSiblingIndicesRange.max);
	}
}
#endif