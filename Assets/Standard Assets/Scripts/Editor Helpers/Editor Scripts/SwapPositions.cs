#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using Extensions;

public class SwapPositions : EditorScript
{
	public override void Do ()
	{
		Transform[] transforms = new Transform[0];
		transforms = transforms.Add(Selection.transforms[0]);
		transforms = transforms.Add(Selection.transforms[1]);
		Vector3 previousPosition = transforms[0].position;
		transforms[0].position = transforms[1].position;
		transforms[1].position = previousPosition;
		DestroyImmediate(Selection.transforms[0].GetComponent<SwapPositions>());
		DestroyImmediate(Selection.transforms[1].GetComponent<SwapPositions>());
	}
}
#endif