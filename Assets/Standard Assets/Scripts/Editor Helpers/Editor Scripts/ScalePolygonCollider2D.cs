﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

public class ScalePolygonCollider2D : EditorScript
{
	public PolygonCollider2D polygonCollider;
	public Vector2 scale;
	Vector2[] points;

	public override void Do ()
	{
		if (polygonCollider == null)
			polygonCollider = GetComponent<PolygonCollider2D>();
		points = polygonCollider.points;
		for (int i = 0; i < points.Length; i ++)
			points[i] = points[i].Multiply(scale);
		polygonCollider.points = points;
	}
}
#endif