﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MinigamesVR
{
	public interface IDestructable
	{
		float Hp { get; set; }
		uint MaxHp { get; set; }
		
		void TakeDamage (float amount);
		void Death ();
	}
}