using System.Collections.Generic;

public interface ICompactable<T1, T2>
{
    T2 Compact (IEnumerable<T1> enumerable);
    IEnumerable<T1> Uncompact (T2 compactedObject);
}