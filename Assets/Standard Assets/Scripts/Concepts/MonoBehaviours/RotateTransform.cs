using UnityEngine;
using Extensions;
using MinigamesVR;

public class RotateTransform : UpdateWhileEnabled
{
	public float rate;
	public Transform trs;

	public override void DoUpdate ()
	{
		trs.eulerAngles += Vector3.forward * rate * Time.deltaTime;
	}
}