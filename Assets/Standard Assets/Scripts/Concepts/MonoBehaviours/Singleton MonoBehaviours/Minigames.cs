using MinigamesVR;

public class Minigames : SingletonMonoBehaviour<Minigames>
{
	public int lives;

	public void Lose ()
	{
		_SceneManager.instance.RestartSceneWithoutTransition ();
	}
}