﻿using UnityEngine;
using Extensions;
using System;

namespace MinigamesVR
{
	[RequireComponent(typeof(AudioSource))]
	public class SoundEffect : Spawnable
	{
		public AudioSource audioSource;

		[Serializable]
		public struct Settings
		{
			public AudioClip clip;
			public float volume;
			public float pitch;

			public Settings (AudioClip clip) : this (clip, 1, 1)
			{
			}

			public Settings (AudioClip clip, float volume, float pitch)
			{
				this.clip = clip;
				this.volume = volume;
				this.pitch = pitch;
			}
		}
	}
}