using System;
using UnityEngine;

namespace MinigamesVR
{
	[Serializable]
	public class Team<T>
	{
		public T representative;
		public T[] representatives = new T[0];
		public Color color;
		public Material material;
		public Team<T> opponent;
		public Team<T>[] opponents = new Team<T>[0];
	}
}