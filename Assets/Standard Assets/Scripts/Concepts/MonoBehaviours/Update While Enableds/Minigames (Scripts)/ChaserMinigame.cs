using UnityEngine;

public class ChaserMinigame : Minigame
{
	public float moveSpeed;
	public Transform chaserTrs;
	public Transform targetTrs;
	public Transform boundaryTrs;

	public override void DoUpdate ()
	{
		base.DoUpdate ();
		chaserTrs.position += (targetTrs.position - chaserTrs.position).normalized * moveSpeed * Time.deltaTime;
		float combinedRadius = chaserTrs.lossyScale.x / 2 + targetTrs.lossyScale.x / 2;
		if ((chaserTrs.position - targetTrs.position).sqrMagnitude <= combinedRadius * combinedRadius)
			Reset ();
	}

	public override void Reset ()
	{
		base.Reset ();
		chaserTrs.position = trs.position + (Random.onUnitSphere * (boundaryTrs.lossyScale.x / 2 - chaserTrs.lossyScale.x / 2));
	}
}