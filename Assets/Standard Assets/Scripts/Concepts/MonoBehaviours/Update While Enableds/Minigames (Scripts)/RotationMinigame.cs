using UnityEngine;

public class RotationMinigame : Minigame
{
	public float rotateRate;

	public override void DoUpdate ()
	{
		base.DoUpdate ();
		if (!isGrabbed)
		{
			trs.forward = Vector3.RotateTowards(trs.forward, Vector3.down, rotateRate * Time.deltaTime, 0);
			if (trs.forward == Vector3.down)
				Reset ();
		}
	}

	public override void Reset ()
	{
		base.Reset ();
		Vector3 forward = Vector3.up;
		Vector3 up = Random.onUnitSphere;
		Vector3.OrthoNormalize(ref forward, ref up);
		trs.rotation = Quaternion.LookRotation(forward, up);
	}
}