using UnityEngine;
using MinigamesVR;

public class Minigame : UpdateWhileEnabled
{
	public Transform trs;
	public float grabbableBorderRadius;
	protected bool isGrabbed;

	public override void DoUpdate ()
	{
		float distanceSqr = (VRCameraRig.instance.leftHand.trs.position - trs.position).sqrMagnitude;
		float radius = trs.lossyScale.x / 2;
		if (distanceSqr <= (radius + grabbableBorderRadius) * (radius + grabbableBorderRadius) && distanceSqr >= (radius - grabbableBorderRadius) * (radius - grabbableBorderRadius))
		{
			if (VRCameraRig.instance.leftHand.gripInput)
				trs.SetParent(VRCameraRig.instance.leftHand.trs);
			else if (VRCameraRig.instance.rightHand.gripInput)
				trs.SetParent(VRCameraRig.instance.rightHand.trs);
			else
				trs.SetParent(null);
		}
	}

	public virtual void Reset ()
	{
		Minigames.instance.lives --;
		if (Minigames.instance.lives <= 0)
			Minigames.instance.Lose ();
	}
}