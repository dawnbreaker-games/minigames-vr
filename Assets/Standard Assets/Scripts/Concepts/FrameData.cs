using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace MinigamesVR
{
	public class FrameData
	{
		public int frame;
		public UpdatableEntry[] updatableEntries = new UpdatableEntry[0];

		[Serializable]
		public struct UpdatableEntry
		{
			public IUpdatable updatable;
			public float time;
		}

		public static FrameData Sample ()
		{
			throw new Exception("FrameData.Sample is not finished being implemented");
			for (int i = 0; i < GameManager.updatables.Length; i ++)
			{
				IUpdatable updatable = GameManager.updatables[i];
				float previousTime = Time.realtimeSinceStartup;
				updatable.DoUpdate ();
				// print((updatable as MonoBehaviour).name + " " + (Time.realtimeSinceStartup - previousTime));
			}
		}
	}
}